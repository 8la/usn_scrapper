# coding: utf-8
import requests
from bs4 import BeautifulSoup


class USNSite:
    """Ubuntu Security Notices site"""

    url = "https://www.ubuntu.com/usn/"
    release = "trusty"
    notices = []

    def __init__(self, url, release):

        self.url = url
        self.release = release

    def scrap(self, amount=1):

        html = requests.get(self.url + self.release).text
        print(html)
        soup = BeautifulSoup(html)
        print(soup.find_all('a'))


class USNnotice:
    """Ubuntu Notice item."""

    def __init__(self, url, release):

        if url is None or release is None:
            print("Error, url and release param's are necessary")
        self.url = url
        self.release = release
